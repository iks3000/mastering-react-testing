module.exports = {
    mode: 'development',
    devServer: {
        open: false,
        port: 8080,
        proxy: [{
            context: ['/community', '/subscribe', '/unsubscribe', '/analytics/user', '/analytics/performance'],
            target: 'http://localhost:3000',
        }],
    },
};