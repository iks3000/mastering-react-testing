import { getUsersReducer, initialState } from "./getUsersReducer";

describe("get users reducer", () => {
  it("should return the initial state", () => {
    expect(getUsersReducer(undefined, {})).toMatchSnapshot();
  });

  it("should handle GET_USERS_START", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERS_START",
      })
    ).toMatchSnapshot();
  });

  it("should handle GET_USERS_SUCCESS", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERS_SUCCESS",
        users: [
          {
            id: "1",
            avatar: "some url",
            description: "description text",
            firstName: "John",
            lastName: "Hardman",
            position: "Developer",
          },
        ],
      })
    ).toMatchSnapshot();
  });
  it("should handle GET_USERS_FAILURE", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERS_FAILURE",
        error: "error message",
      })
    ).toMatchSnapshot();
  });

  //get users by id

  it("should handle GET_USERSBYID_START", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERSBYID_START",
      })
    ).toMatchSnapshot();
  });

  it("should handle GET_USERSBYID_SUCCESS", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERSBYID_SUCCESS",
        users: [
          {
            id: "1",
            avatar: "some url",
            description: "description text",
            firstName: "John",
            lastName: "Hardman",
            position: "Developer",
          },
        ],
      })
    ).toMatchSnapshot();
  });
  it("should handle GET_USERSBYID_FAILURE", () => {
    expect(
      getUsersReducer(initialState, {
        type: "GET_USERSBYID_FAILURE",
        error: "error message",
      })
    ).toMatchSnapshot();
  });
});
