export const GET_USERS_START = 'GET_USERS_START';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'GET_USERS_FAILURE';

export const GET_USERSBYID_START = 'GET_USERSBYID_START';
export const GET_USERSBYID_SUCCESS = 'GET_USERSBYID_SUCCESS';
export const GET_USERSBYID_FAILURE = 'GET_USERSBYID_FAILURE';

export const getUsersStart = () => {
    return { type: 'GET_USERS_START' };
};

export const getUsersSuccess = (payload) => {
    return { type: 'GET_USERS_SUCCESS', payload };
};

export const getUsersFailure = (payload) => {
    return { type: 'GET_USERS_FAILURE', payload };
};

export const getUsersByIDStart = () => {
    return { type: 'GET_USERSBYID_START' };
};

export const getUsersByIDsSuccess = (payload) => {
    return { type: 'GET_USERSBYID_SUCCESS', payload };
};

export const getUsersByIDFailure = (payload) => {
    return { type: 'GET_USERSBYID_FAILURE', payload };
};