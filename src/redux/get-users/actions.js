import {
  getUsersStart,
  getUsersSuccess,
  getUsersFailure,
  getUsersByIDStart,
  getUsersByIDsSuccess,
  getUsersByIDFailure,
} from './actionTypes';

import axios from 'axios';

export const getUsers = () => {
  return async (dispatch) => {
    try {
      dispatch(getUsersStart());
      const users = await axios.get('/community');
      dispatch(getUsersSuccess(users.data));
    } catch (error) {
      dispatch(getUsersFailure(error?.response?.data?.error));
    }
  };
};

export const getUsersByID = (userID) => {
  return async (dispatch) => {
    try {
      dispatch(getUsersByIDStart());
      const user = await axios.get(`/community/${userID}`);
      dispatch(getUsersByIDsSuccess(user.data));
    } catch (error) {
      dispatch(getUsersByIDFailure(error));
    }
  };
};
