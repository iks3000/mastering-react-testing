import {
    GET_USERS_START,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE,
    GET_USERSBYID_START,
    GET_USERSBYID_SUCCESS,
    GET_USERSBYID_FAILURE,
} from './actionTypes';

export const initialState = {
    users: [],
    user: null,
    isLoading: false,
    error: '',
};

export const getUsersReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_USERS_START:
            return {
                isLoading: true,
            };
        case GET_USERS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                users: payload,
            };
        case GET_USERS_FAILURE:
            return {
                ...state,
                error: payload,
                isLoading: false,
            };
        case GET_USERSBYID_START:
            return {
                isLoading: true,
            };
        case GET_USERSBYID_SUCCESS:
            return {
                ...state,
                user: payload,
                isLoading: false,
            };
        case GET_USERSBYID_FAILURE:
            return {
                ...state,
                error: payload,
                isLoading: false,
            };
        default:
            return state;
    }
};