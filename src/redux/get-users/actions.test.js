import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import axios from 'axios';
import {
    getUsersByIDFailure,
    getUsersByIDsSuccess,
    getUsersByIDStart,
    getUsersFailure,
    getUsersStart,
    getUsersSuccess,
} from './actionTypes';
import { getUsers, getUsersByID } from './actions';
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

// get-users test

describe('get users thunk', () => {
    it('dispatches get users and error on call when API is not mocked', async() => {
        const store = mockStore({});
        const requestUsersStart = getUsersStart();
        const GetUsersFailureDispatch = getUsersFailure('Error Message');

        await store.dispatch(await getUsers());
        const actionsResulted = store.getActions();
        const expectedActions = [requestUsersStart, GetUsersFailureDispatch];
        expect(actionsResulted.length).toEqual(expectedActions.length);
        expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
        expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
    });

    it('dispatches get users and success on call when API is mocked', async() => {
        const store = mockStore({});
        const requestUsersStart = getUsersStart();
        const GetUsersSuccessDispatch = getUsersSuccess('Mock Data');
        jest
            .spyOn(axios, 'get')
            .mockResolvedValue({ status: 200, data: 'Mock Data' });

        await store.dispatch(await getUsers());
        const actionsResulted = store.getActions();
        const expectedActions = [requestUsersStart, GetUsersSuccessDispatch];
        expect(actionsResulted.length).toEqual(expectedActions.length);
        expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
        expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
    });
});

// get-users-by-id test

describe('get users by id thunk', () => {
    it('dispatches get users by id and error on call when API is not mocked', async() => {
        const store = mockStore({});
        const requestUsersByIdStart = getUsersByIDStart();
        const GetUsersByIdFailureDispatch = getUsersByIDFailure('Error Message');

        await store.dispatch(await getUsersByID());
        const actionsResulted = store.getActions();
        const expectedActions = [
            requestUsersByIdStart,
            GetUsersByIdFailureDispatch,
        ];
        expect(actionsResulted.length).toEqual(expectedActions.length);
        expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
        expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
    });

    it('dispatches get users by id and success on call when API is mocked', async() => {
        const store = mockStore({});
        const requestUsersByIdStart = getUsersByIDStart();
        const GetUsersByIdSuccessDispatch = getUsersByIDsSuccess('Mock Data');
        jest
            .spyOn(axios, 'get')
            .mockResolvedValue({ status: 200, data: 'Mock Data' });

        await store.dispatch(await getUsersByID());
        const actionsResulted = store.getActions();
        const expectedActions = [
            requestUsersByIdStart,
            GetUsersByIdSuccessDispatch,
        ];
        expect(actionsResulted.length).toEqual(expectedActions.length);
        expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
        expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
    });
});