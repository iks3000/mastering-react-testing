import {
  initialState,
  sectionVisibilityReducer,
} from "./sectionVisibilityReducer";

describe("section show reducer", () => {
  it("should return the initial state", () => {
    expect(sectionVisibilityReducer(undefined, {})).toMatchSnapshot();
  });

  it("should handle SHOW_SECTION", () => {
    expect(
      sectionVisibilityReducer(initialState, {
        type: "SHOW_SECTION",
      })
    ).toMatchSnapshot();
  });
});
