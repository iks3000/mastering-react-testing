import { showSection, SHOW_SECTION } from './actions';

describe('show/hide section', () => {
    it('dispatches how/hide section', () => {
        const expectedAction = [{ type: SHOW_SECTION }];

        showSection((receivedAction) => {
            expect(receivedAction).toEqual(expectedAction);
        });
    });
});