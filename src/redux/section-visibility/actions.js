export const SHOW_SECTION = 'SHOW_SECTION';
export const HIDE_SECTION = 'HIDE_SECTION';

export const showSection = () => ({ type: 'SHOW_SECTION' });