import {
  subscribeStart,
  subscribeSuccess,
  subscribeFailure,
  unSubscribeSuccess,
  unSubscribeFailure,
} from './actionTypes';

import axios from 'axios';

export const subscribe = (email) => {
  return async (dispatch) => {
    try {
      dispatch(subscribeStart());
      const subscribeResponse = await axios.post('/subscribe', email);
      dispatch(subscribeSuccess(subscribeResponse.status));
      return subscribeResponse;
    } catch (error) {
      dispatch(subscribeFailure(error?.response?.data?.error));
    }
  };
};

export const unSubscribe = () => {
  return async (dispatch) => {
    try {
      dispatch(subscribeStart());
      const unSubscribeResponse = await axios.post('/unsubscribe');
      dispatch(unSubscribeSuccess(unSubscribeResponse.data));
    } catch (error) {
      dispatch(unSubscribeFailure(error));
    }
  };
};
