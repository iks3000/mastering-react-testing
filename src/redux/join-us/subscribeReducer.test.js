import { initialState, subscribeReducer } from "./subscribeReducer";

describe("subscribe reducer", () => {
  it("should return the initial state", () => {
    expect(subscribeReducer(undefined, {})).toMatchSnapshot();
  });

  it("should handle SUBSCRIBE_START", () => {
    expect(
      subscribeReducer(initialState, {
        type: "SUBSCRIBE_START",
      })
    ).toMatchSnapshot();
  });

  it("should handle SUBSCRIBE_SUCCESS", () => {
    expect(
      subscribeReducer(initialState, {
        type: "SUBSCRIBE_SUCCESS",
        status: 200,
      })
    ).toMatchSnapshot();
  });
  it("should handle SUBSCRIBE_FAILURE", () => {
    expect(
      subscribeReducer(initialState, {
        type: "SUBSCRIBE_FAILURE",
        error: "error message",
      })
    ).toMatchSnapshot();
  });

  // unsubscribe

  it("should handle unsunbscribe SUBSCRIBE_START", () => {
    expect(
      subscribeReducer(initialState, {
        type: "SUBSCRIBEE_START",
      })
    ).toMatchSnapshot();
  });

  it("should handle UNSUBSCRIBE_SUCCESS", () => {
    expect(
      subscribeReducer(initialState, {
        type: "UNSUBSCRIBE_SUCCESS",
        subscribed: false,
      })
    ).toMatchSnapshot();
  });
  it("should handle UNSUBSCRIBE_FAILURE", () => {
    expect(
      subscribeReducer(initialState, {
        type: "UNSUBSCRIBE_FAILURE",
        error: "error message",
      })
    ).toMatchSnapshot();
  });
});
