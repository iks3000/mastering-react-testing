import thunk from "redux-thunk";
import configureStore from "redux-mock-store";

import axios from "axios";
import {
  subscribeFailure,
  subscribeStart,
  subscribeSuccess,
  unSubscribeFailure,
  unSubscribeStart,
  unSubscribeSuccess,
} from "./actionTypes";
import { subscribe, unSubscribe } from "./actions";
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

// subscribe test

describe("subscribed thunk", () => {
  it("dispatches subscribed and error on call when API is not mocked", async () => {
    const store = mockStore({});
    const requestSubscribeStart = subscribeStart();
    const subscribeFailureDispatch = subscribeFailure("Error Message");

    await store.dispatch(await subscribe());
    const actionsResulted = store.getActions();
    const expectedActions = [requestSubscribeStart, subscribeFailureDispatch];
    expect(actionsResulted.length).toEqual(expectedActions.length);
    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
    expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
  });

  it("dispatches subscribed and success on call when API is mocked", async () => {
    const store = mockStore({});
    const requestSubscribeStart = subscribeStart();
    const subscribeSuccessDispatch = subscribeSuccess("Mock Data");
    jest
      .spyOn(axios, "post")
      .mockResolvedValue({ status: 200, data: "Mock Data" });

    await store.dispatch(await subscribe());
    const actionsResulted = store.getActions();
    const expectedActions = [requestSubscribeStart, subscribeSuccessDispatch];
    expect(actionsResulted.length).toEqual(expectedActions.length);
    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
    expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
  });
});

// unsubscribe test

describe("unsubscribed thunk", () => {
  it("dispatches unsubscribed and error on call when API is not mocked", async () => {
    const store = mockStore({});
    const requestUnSubscribeStart = unSubscribeStart();
    const unSubscribeFailureDispatch = unSubscribeFailure("Error Message");

    await store.dispatch(await unSubscribe());
    const actionsResulted = store.getActions();
    const expectedActions = [
      requestUnSubscribeStart,
      unSubscribeFailureDispatch,
    ];
    expect(actionsResulted?.length).toEqual(expectedActions?.length);
    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
    expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
  });

  it("dispatches unsubscribed and success on call when API is mocked", async () => {
    const store = mockStore({});
    const requestUnSubscribeStart = unSubscribeStart();
    const unSubscribeSuccessDispatch = unSubscribeSuccess("Mock Data");
    jest
      .spyOn(axios, "post")
      .mockResolvedValue({ status: 200, data: "Mock Data" });

    await store.dispatch(await unSubscribe());
    const actionsResulted = store.getActions();
    const expectedActions = [
      requestUnSubscribeStart,
      unSubscribeSuccessDispatch,
    ];
    expect(actionsResulted.length).toEqual(expectedActions.length);
    expect(actionsResulted[0].type).toEqual(expectedActions[0].type);
    expect(actionsResulted[1].type).toEqual(expectedActions[1].type);
  });
});
