import {
  SUBSCRIBE_START,
  SUBSCRIBE_SUCCESS,
  SUBSCRIBE_FAILURE,
  UNSUBSCRIBE_SUCCESS,
  UNSUBSCRIBE_FAILURE,
} from "./actionTypes";

export const initialState = {
  status: null,
  isLoading: false,
  subscribed: false,
  error: "",
};

export const subscribeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SUBSCRIBE_START:
      return {
        isLoading: true,
      };
    case SUBSCRIBE_SUCCESS:
      return {
        ...state,
        status: payload,
        subscribed: true,
        isLoading: false,
      };
    case SUBSCRIBE_FAILURE:
      return {
        ...state,
        error: payload,
        isLoading: false,
      };
    case UNSUBSCRIBE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        subscribed: false,
      };
    case UNSUBSCRIBE_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: payload,
      };
    default:
      return state;
  }
};
