import { combineReducers } from 'redux';
import { getUsersReducer } from './get-users/getUsersReducer';
import { subscribeReducer } from './join-us/subscribeReducer';
import { sectionVisibilityReducer } from './section-visibility/sectionVisibilityReducer';

const rootReducer = combineReducers({
    users: getUsersReducer,
    success: subscribeReducer,
    visible: sectionVisibilityReducer,
});

export default rootReducer;