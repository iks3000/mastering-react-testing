import { rest } from 'msw';

const mockUsers = [
  {
    id: '2f1b6bf3-f23c-47e4-88f2-e4ce89409376',
    avatar: 'http://localhost:3000/avatars/avatar1.png',
    firstName: 'Mary',
    lastName: 'Smith',
    position: 'Lead Designer at Company Name',
    description:
      'Ullamco laboris nisi ut, ut enim ad minim veniam, quis nostrud exercitation.',
  },
  {
    id: '1157fea1-8b72-4a9e-b253-c65fa1556e26',
    avatar: 'http://localhost:3000/avatars/avatar2.png',
    firstName: 'Bill',
    lastName: 'Filler',
    position: 'Lead Engineer at Company Name',
    description: 'Sed do eiusmod tempor incididunt ut labore et dolor.',
  },
  {
    id: 'b96ac290-543c-4403-80fe-0c2d44e84ea9',
    avatar: 'http://localhost:3000/avatars/avatar3.png',
    firstName: 'Tim',
    lastName: 'Gates',
    position: 'CEO at Company Name',
    description:
      'Reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  },
];

const mockUser = {
  id: '2f1b6bf3-f23c-47e4-88f2-e4ce89409376',
  avatar: 'http://localhost:3000/avatars/avatar1.png',
  firstName: 'Mary',
  lastName: 'Smith',
  position: 'Lead Designer at Company Name',
  description:
    'Ullamco laboris nisi ut, ut enim ad minim veniam, quis nostrud exercitation.',
};

const mockId = '2f1b6bf3-f23c-47e4-88f2-e4ce89409376';

export const handlers = [
  rest.get('/community', async (req, res, ctx) => {
    const originalResponse = await ctx.fetch(req);
    console.log(originalResponse.status);

    if (originalResponse.status === 404) {
      return res(
        ctx.status(404),
        ctx.json({ message: 'Something went wrong' })
      );
    }

    return res(ctx.status(200), ctx.json(mockUsers));
  }),

  rest.get(`/community/${mockId}`, (req, res, ctx) => {
    if (!res) {
      return res(ctx.status(404));
    }
    return res(ctx.status(200), ctx.json(mockUser));
  }),

  rest.post('/subscribe', async (req, res, ctx) => {
    const originalResponse = await ctx.fetch(req);
    if (originalResponse.status === 404) {
      return res(
        ctx.status(404),
        ctx.json({ message: 'Something went wrong' })
      );
    }

    return res(ctx.status(200), ctx.json());
  }),

  rest.post('/unsubscribe', async (req, res, ctx) => {
    const originalResponse = await ctx.fetch(req);
    if (originalResponse.status === 404) {
      return res(
        ctx.status(404),
        ctx.json({ message: 'Something went wrong' })
      );
    }
    return res(ctx.status(200), ctx.json());
  }),
];
