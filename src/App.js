import GetUsers from './components/GetUsers/GetUsers';
import JoinUs from './components/JoinUs/JoinUs';
import MainPage from './components/MainPage/MainPage';
import UserDetails from './components/UserDetails/UserDetails';
import NotFound from './components/NotFound/NotFound';
import { HashRouter as Router, Switch, Route, Link } from 'react-router-dom';

function App({ visible }) {
  return (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <ul className="navbar-nav mr-auto">
          <li><Link to={'/'} className="nav-link">Main</Link></li>
          <li><Link to={'/community'} className="nav-link">Community</Link></li>
          <li><Link to={'/user'} className="nav-link">Join Our Program</Link></li>
          </ul>
        </nav>
        <Switch>
          <Route exact path='/' component={MainPage}/>
          <Route exact path='/community' component={GetUsers} />
          <Route exact path='/community/:id' component={UserDetails} />
          <Route exact path='/user' component={JoinUs} />
          <Route>
          <NotFound />
          </Route>
        </Switch>
      </Router>
  );
}

export default App;
