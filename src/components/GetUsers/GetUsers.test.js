import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import GetUsers from './GetUsers';
import { getUsers } from '../../redux/get-users/actions';
import rootReducer from './../../redux/rootReducer';
import { shallow } from 'enzyme';

describe('<GetUsers /> unit test', () => {
  const getWrapper = (
    mockStore = createStore(rootReducer, { visible: false })
  ) =>
    mount(
      <Provider store={mockStore}>
        <GetUsers />
      </Provider>
    );

  it('should dispatch the correct action on button click', () => {
    const mockStore = createStore(rootReducer, { visible: false });
    mockStore.dispatch = jest.fn();

    const wrapper = getWrapper(mockStore);
    wrapper.find('button').simulate('click');
    expect(mockStore.dispatch).toHaveBeenCalledWith(getUsers());
  });

  it('Test click event', () => {
    const mockCallBack = jest.fn();

    const button = shallow(<button onClick={mockCallBack}>Ok!</button>);
    button.find('button').simulate('click');
    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
