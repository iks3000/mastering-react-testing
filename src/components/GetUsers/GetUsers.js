import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { showSection } from '../../redux/section-visibility/actions';
import { getUsers } from './../../redux/get-users/actions';
import { Link } from 'react-router-dom';
import Loader from '../Loader/Loader';

import './GetUsers.scss';

const GetUsers = () => {
  const dispatch = useDispatch();

  const users = useSelector((state) => state.users?.users);
  const isLoading = useSelector((state) => state.users?.isLoading);
  const error = useSelector((state) => state.users?.error);
  const visible = useSelector((state) => state.visible?.visible);

  const handleToggle = () => {
    dispatch(showSection());
  };

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const UserTemplate = () => {
    return (
      <div className='wrapper'>
        <div className='hide-warpper'>
          <h2 className='title'>
            Big Community of <br /> People Like You
          </h2>
          <button onClick={handleToggle} className='btn-hide-show'>
            {visible ? 'Hide section' : 'Show section'}
          </button>
        </div>
        {visible && (
          <>
            <p className='description'>
              We’ re proud of our products, and we’ re really excited <br />
              when we get feedback from our users.
            </p>
            <div className='get-user__users-container'>
              {users?.map((user) => (
                <div className='users' key={user.id}>
                  <img className='users-img' src={user.avatar} alt='img' />
                  <p className='users-description'>{user.description}</p>
                  <p className='users-name'>
                    <Link className='user-link' to={`/community/${user.id}`}>
                      {user.firstName} {user.lastName}
                    </Link>
                  </p>
                  <p className='users-position'>{user.position}</p>
                </div>
              ))}
            </div>
          </>
        )}
      </div>
    );
  };

  return (
    <section className='get-user'>
      {error && <pre> {JSON.stringify(error, null, 2)} </pre>}
      {isLoading && <Loader />}
      {users && <UserTemplate dataUrl={users} />}
    </section>
  );
};

export default GetUsers;
