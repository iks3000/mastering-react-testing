import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import JoinUs from './JoinUs';
import { subscribe } from '../../redux/join-us/actions';
import rootReducer from './../../redux/rootReducer';

describe('<JoinUs /> unit test', () => {
  const getWrapper = (
    mockStore = createStore(rootReducer, { success: false })
  ) =>
    mount(
      <Provider store={mockStore}>
        <JoinUs />
      </Provider>
    );

  it('should dispatch the correct action on button click', () => {
    const mockStore = createStore(rootReducer, { success: true });
    mockStore.dispatch = jest.fn();

    const wrapper = getWrapper(mockStore);
    wrapper.find('button').simulate('click');
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      subscribe('test@gmail.com')
    );
  });
});
