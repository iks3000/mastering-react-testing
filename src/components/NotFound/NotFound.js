import React from 'react';
import { Link } from 'react-router-dom';
import backIcon from '../../images/left-arrow.svg';
import './NotFound.scss';

const NotFound = () => {
  return (
    <div className='not-found'>
      <h1 className='not-found__title'> Page Not Found </h1>
      <p className='not-found__description'>
        Looks like you've followed a broken link or entered a URL that doesn't
        exist on this site.
      </p>
      <Link to='/' className='not-found__link'>
        <div className='back-icon'>
          <img src={backIcon} alt='back-icon' />
        </div>
        <p>Back to our site</p>
      </Link>
    </div>
  );
};

export default NotFound;
