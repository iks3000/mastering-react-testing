import React from "react";
import { shallow } from "enzyme";
import Loader from "./Loader";

describe("Loader", () => {
  it('should render correctly in "debug" mode', () => {
    const component = shallow(<Loader debug />);

    expect(component).toMatchSnapshot();
  });
});
